#!/bin/bash

protoc -I. -I /usr/include/ -I ../nanopb/generator/proto/ -I ../protos/ -o VGFPlatinum.pb ../protos/VGFPlatinum_nano.proto
python ../nanopb/generator/nanopb_generator.py VGFPlatinum.pb
mv *.pb.h include
mv *.pb.c src

rm *.pb
