/*
 * onewire.c
 *
 *  Created on: 12 нояб. 2013 г.
 *      Author: sony
 */


#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include <string.h>
#include <stdio.h>
#include "hardware.h"
//#include "controller.pb.h"
#include "SEGGER_RTT.h"
#include "onewire.h"


static DS12B20Device Devices[DS12B80_COUNT] =
	{{0, {0x28, 0x7a, 0xa3, 0x3e, 0x05, 0x00, 0x00, 0x5c}, 0.0f},
	 {1, {0x28, 0x6f, 0x29, 0x32, 0x05, 0x00, 0x00, 0x88}, 0.0f},
	 {2, {0x28, 0x05, 0xa3, 0x36, 0x05, 0x00, 0x00, 0xfc}, 0.0f},
	 {3, {0x28, 0xb8, 0x27, 0x32, 0x05, 0x00, 0x00, 0x6b}, 0.0f},
	 {4, {0x28, 0xeb, 0x22, 0x32, 0x05, 0x00, 0x00, 0xce}, 0.0f},
	 {5, {0x28, 0xca, 0x39, 0x32, 0x05, 0x00, 0x00, 0x43}, 0.0f},
	 {6, {0x28, 0x01, 0x55, 0x31, 0x05, 0x00, 0x00, 0x9c}, 0.0f},
	 {7, {0x28, 0xbe, 0xba, 0x31, 0x05, 0x00, 0x00, 0x0a}, 0.0f},
	 {8, {0x28, 0x91, 0x72, 0x31, 0x05, 0x00, 0x00, 0x84}, 0.0f}
};

__IO uint8_t TxBuffer[8];
uint8_t RxBuffer[8];


void OneWire::OW_Init() {
	/* Enable USART2 CLK */
	WIRE_USART_CLK_INIT(WIRE_USART_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);

	GPIO_PinAFConfig(WIRE_USART_TX_GPIO_PORT, WIRE_USART_TX_SOURCE, WIRE_USART_TX_AF);
	GPIO_PinAFConfig(WIRE_USART_RX_GPIO_PORT, WIRE_USART_RX_SOURCE, WIRE_USART_RX_AF);

GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = WIRE_USART_TX_PIN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(WIRE_USART_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = WIRE_USART_RX_PIN;
	GPIO_Init(WIRE_USART_RX_GPIO_PORT, &GPIO_InitStructure);

USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = BAUD_RATE_NORMAL;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(WIRE_USART, &USART_InitStructure);
	USART_Cmd(WIRE_USART, ENABLE);
}


/**
 *
 */
uint8_t OneWire::OW_ReadByte() {
	uint8_t data = 0x00;
	for (int i = 0; i < 8; i++) {
		USART_SendData(WIRE_USART, 0xFF);
		while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_TC) == RESET) {
			vTaskDelay(1);
		}
		while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_RXNE) == RESET) {
			vTaskDelay(1);
		}

		uint8_t ret = USART_ReceiveData(WIRE_USART);
		data >>= 1;
		if (ret == 255)
			data |= 0x80;
	}
	return data;
}

uint8_t OneWire::OW_ComputeCRC8(unsigned char inData, unsigned char seed)
{
	uint8_t bitsLeft;
	uint8_t temp;

	for (bitsLeft = 8; bitsLeft > 0; bitsLeft--) {
		temp = ((seed ^ inData) & 0x01);
		if (temp == 0) {
			seed >>= 1;
		} else {
			seed ^= 0x18;
			seed >>= 1;
			seed |= 0x80;
		}
		inData >>= 1;
	}
	return seed;
}


uint8_t OneWire::OW_CalcScratchCRC(uint8_t *data) {
uint8_t crc8 = 0;
	for (int i = 0; i < 8; i++) {
		crc8 = OW_ComputeCRC8(*data, crc8);
		data++;
	}
	return crc8;
}



float OneWire::ReadTemperature(DS12B20Device *device) {
float fTemp;
uint16_t ulTemp;
uint8_t scratch[9];

	OW_Reset();
	OW_WriteByte(OW_ROM_MATCH);
	for (int i = 0; i < 8; i++)
		OW_WriteByte(device->ROM[i]);

	OW_WriteByte(0xBE); // Read scratch
	for (int i = 0; i < 9; i++)
		scratch[i] = OW_ReadByte();

	if (OW_CalcScratchCRC(scratch) != scratch[8]) {
		device->Temperature = 0.2f;
		return 0.0f;
	}

	ulTemp = scratch[0];
	ulTemp |= scratch[1] << 8;
	if (ulTemp > 2097) {
		ulTemp = 65535 - ulTemp;
		fTemp = -(((ulTemp & 0x7F0) >> 4)*1.0f + ((ulTemp & 0xF)*0.0625f));
	} else {
		fTemp = ((ulTemp & 0x7F0) >> 4)*1.0f + (ulTemp & 0xF)*0.0625f;
	}

	device->Temperature = fTemp;
	return fTemp;
}

OneWire::OneWire(char const *name, unsigned portBASE_TYPE priority, uint16_t stackDepth) {
	xMutex = xSemaphoreCreateMutex();
	assert_param(xMutex);

	xTaskCreate(task_onewire, name, stackDepth, this, priority, &handle);
	assert_param(handle);
}


/**
 * @function OW_Reset
 * @brief Сброс шины 1-Wire
 * @param None
 * @retval OW_DEVICES_YES - если есть устройства на шине, OW_DEVICES_NO - в противном случае
 */
uint8_t OneWire::OW_Reset() {
USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = BAUD_RATE_RESET;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(WIRE_USART, &USART_InitStructure);
	USART_ClearFlag(WIRE_USART, USART_FLAG_TC);

	USART_SendData(WIRE_USART, 0xF0);
	while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_TC) == RESET) {
//		vTaskDelay(1);
	}

uint8_t ret = USART_ReceiveData(WIRE_USART);
	USART_InitStructure.USART_BaudRate = BAUD_RATE_NORMAL;
	USART_Init(WIRE_USART, &USART_InitStructure);
	USART_ClearFlag(WIRE_USART, USART_FLAG_TC);

	if (ret != 0xF0)
		return OW_DEVICES_YES; /* Есть устройство! */
	return OW_DEVICES_NO; /* Нет устройства */
}


void OneWire::OW_WriteByte(uint8_t data) {
	for (int i = 0; i < 8; i++) {
		if (data & 0x01)
			USART_SendData(WIRE_USART, 0xFF); // Запись 1
		else
			USART_SendData(WIRE_USART, 0x00); // Запись 0

		while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_TC) == RESET) {
//			vTaskDelay(1);
		}
		data >>= 1;
	}
}

bool OneWire::GetTemperatures(float *T) {
bool ret = false;
	if (xSemaphoreTake(xMutex, 200) == pdTRUE) {
		for (uint32_t i = 0; i < sizeof(Devices)/sizeof(DS12B20Device); i++) {
			T[i] = Devices[i].Temperature;
		}
		ret = true;
		xSemaphoreGive(xMutex);
	}
	return ret;
}

void OneWire::task() {
	OW_Init();

TickType_t xLastWakeUp = xTaskGetTickCount();

	for (;;) {
		OW_Reset();
		OW_WriteByte(OW_ROM_SKIP);
		OW_WriteByte(0x44);

		vTaskDelayUntil(&xLastWakeUp, 1100);

		if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
			SEGGER_RTT_printf(0, "\n===========================================\n");
			for (uint32_t i = 0; i < sizeof(Devices)/sizeof(DS12B20Device); i++) {
				ReadTemperature(&Devices[i]);

//				char buf[24];
//				FloatToString(buf, Devices[i].Temperature, 2);
//				SEGGER_RTT_printf(0, "Device %d = %s\n", i, buf);
			}
			SEGGER_RTT_printf(0, "===========================================\n\n");
			xSemaphoreGive(xMutex);
		}

		LED2_PORT->ODR ^= LED2_PIN;
	}
}
