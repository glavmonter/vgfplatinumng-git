/*
 *  @file VGFThermo/pid.c
 *  @date 14.01.2013
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief Процесс расчета ПИД регулятора и обслуживания таймеров
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <timers.h>
#include <arm_math.h>
#include <core_cm4.h>
#include <diag/Trace.h>
#include "hardware.h"
#include "pb.h"
#include "pb_encode.h"
#include "pid.h"
#include "canmsg.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "Thermocouple.h"
#include "iadc.h"
#include "SEGGER_RTT.h"


static CANTask *canTask = NULL;

static uint32_t crc32(const unsigned char *buf, size_t len);
static IADC *iADC = NULL;

uint8_t sendTime = pdFALSE;

/**
 * @function PIDInit
 * @brief Запуск таймеров
 * @param None
 * @retval None
 */
void PIDInit() {
	iADC = new IADC("IADC", tskIDLE_PRIORITY, configMINIMAL_STACK_SIZE*2);
	IADCLink(iADC);
}


void vTimerCallback(TimerHandle_t pxTimer) {
(void) pxTimer;
	NVIC_SystemReset();
}

void CANLink(CANTask *l) {
	canTask = l;
}

CANTask::CANTask(char const *name, unsigned portBASE_TYPE priority, uint16_t stackDepth) {
	this->DeviceID = ENCGetID();
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);

	xCanQueueRx = xQueueCreate(4, sizeof(CanRxMsg));
	assert_param(xCanQueueRx);

	InitCan();
	xTaskCreate(task_can, name, stackDepth, this, priority, &handle);
	assert_param(handle);
}

void CANTask::InitCan() {
GPIO_InitTypeDef GPIO_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

	/* CAN1 config */
	GPIO_PinAFConfig(CAN_RX_GPIO_PORT, CAN_RX_SOURCE, CAN_RX_AF);
	GPIO_PinAFConfig(CAN_TX_GPIO_PORT, CAN_TX_SOURCE, CAN_TX_AF);

	GPIO_InitStructure.GPIO_Pin = CAN_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(CAN_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = CAN_RX_PIN;
	GPIO_Init(CAN_RX_GPIO_PORT, &GPIO_InitStructure);

	CAN_CLK_INIT(CAN_CLK, ENABLE);

	CAN_DeInit(CANLIB_CAN);

CAN_InitTypeDef CAN_InitStructure;
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;

	/* 1 MBps ?? */
	CAN_InitStructure.CAN_BS1 = CAN_BS1_14tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_6tq;
	CAN_InitStructure.CAN_Prescaler=4;        //  500 kbit/s
	CAN_Init(CANLIB_CAN, &CAN_InitStructure);

	/* TODO Configure filters */
CAN_FilterInitTypeDef CAN_FilterInitStructure;
	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = GENID(DeviceID, CANMSG_ACK) << 5; // 0x91
	CAN_FilterInitStructure.CAN_FilterIdLow = GENID(DeviceID, CANMSG_NACK) << 5; // 0xA1
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = GENID(DeviceID, CANMSG_DTH) << 5; // 0xC1
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = GENID(DeviceID, CANMSG_HTD) << 5; // 0xB1
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_Filter_FIFO0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	CAN_FilterInitStructure.CAN_FilterNumber = 1;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = GENID(DeviceID, CANMSG_DATA) << 5; // 0xE1
	CAN_FilterInitStructure.CAN_FilterIdLow = GENID(0, CANMSG_SYNC) << 5; // 0x80
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = GENID(0, CANMSG_EMERGENCY) << 5; // 0x70
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = GENID(0, CANMSG_HEARTBEAT) << 5; // 0xD0
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FilterFIFO0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Configure CAN RX0 interrupt. Priority 13 */
	CAN_ITConfig(CANLIB_CAN, CAN_IT_FMP0, ENABLE);
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


CANError CANTask::ReceiveFromHost(CanRxMsg *RxMessage) {

	if (GETMSG(RxMessage->StdId) == CANMSG_HTD) {
		/* Первый пакет, инициализируем переменную количества байт и пакетов */
		m_iBytesToReceive = RxMessage->Data[0];
		m_iBytesToReceive |= RxMessage->Data[1];

		m_iMessagesToReceive = m_iBytesToReceive/7;
		if (m_iBytesToReceive % 7 > 0)
			m_iMessagesToReceive++;
		m_iMaxMessageIndex = m_iMessagesToReceive;

		memcpy(&m_iCrcIn, &RxMessage->Data[2], 4);
		return CANError::CAN_OK;
	}

	if (GETMSG(RxMessage->StdId) == CANMSG_DATA) {
		/* Тут основные данные */
		uint32_t index = RxMessage->Data[0];
		if (index >= m_iMaxMessageIndex) {
			uart_printf("Index number is too large\n");
			return CANError::CAN_INDEX_TO_LARGE;
		}

		memcpy(&m_vBuffer[index*7], &RxMessage->Data[1], RxMessage->DLC - 1);
		m_iMessagesToReceive--;
	}

	if (m_iMessagesToReceive == 0) {
		/* Приняли все сообщения */
		pb_istream_t stream = pb_istream_from_buffer(m_vBuffer, m_iBytesToReceive);

		if (!pb_decode(&stream, Payload_fields, &m_xPayload)) {
			trace_printf("Pb error\n");
			return CANError::CAN_ERROR_PB;
		}
		return CANError::CAN_PKT_READY;
	}
	return CANError::CAN_OK;
}

CANError CANTask::SendToHost() {
uint32_t 	crc;
CanTxMsg 	TxMessage;
CanRxMsg	RxMessage;
uint8_t 	num_messages; // Количество сообщений
size_t		bytes_to_send;

pb_ostream_t stream = pb_ostream_from_buffer(m_vBuffer, Payload_size);
	m_xPayload = Payload_init_default;
	for (int i = 0; i < 3; i++) {
		Pid::DataOut adc_data;
		if (m_lPids[i]->GetState(&adc_data)) {
			m_xPayload.DtH.TemperaturePt[i] = adc_data.Temperature;
			m_xPayload.DtH.SendiagPt[i] = adc_data.sendiag;
			m_xPayload.has_DtH = true;

			m_xPayload.DtH.SendiagPt_count = 3;
			m_xPayload.DtH.TemperaturePt_count = 3;
		}
	}

	m_xPayload.DeviceId = DeviceID;

float T[DS12B80_COUNT];
	if (m_xOneWire->GetTemperatures(T)) {
		for (uint32_t i = 0; i < DS12B80_COUNT; i++) {
			m_xPayload.DtH.TemperatureDs[i] = T[i];
		}
		m_xPayload.DtH.TemperatureDs_count = DS12B80_COUNT;
	}

	memset(m_vBuffer, 0, Payload_size);
	if (!pb_encode(&stream, Payload_fields, &m_xPayload)) {
		return CANError::CAN_ERROR_PB;
	}

	bytes_to_send = stream.bytes_written;

	crc = crc32(m_vBuffer, bytes_to_send);

	/* Сама передача */
	TxMessage.StdId = GENID(DeviceID, CANMSG_DTH);
	TxMessage.ExtId = 0;
	TxMessage.IDE = CAN_Id_Standard;
	TxMessage.RTR = CAN_RTR_Data;
	TxMessage.DLC = 6;

	TxMessage.Data[0] = bytes_to_send & 0xFF;
	TxMessage.Data[1] = (bytes_to_send & 0xFF00) >> 8;
	memcpy(&TxMessage.Data[2], &crc, 4);

	/* TODO Добавить таймаут */
	while (CAN_Transmit(CANLIB_CAN, &TxMessage) == CAN_TxStatus_NoMailBox);

	/* TODO Добавить проверку на входящее сообщение. Если пришло, то вывалиться с ошибкой */
	num_messages = bytes_to_send / 7; // Количество полных сообщений
	for (int i = 0; i < num_messages; i++) {
		TxMessage.StdId = GENID(DeviceID, CANMSG_DATA);
		TxMessage.ExtId = 0;
		TxMessage.IDE = CAN_Id_Standard;
		TxMessage.RTR = CAN_RTR_Data;
		TxMessage.DLC = 8;

		TxMessage.Data[0] = i;
		memcpy(&TxMessage.Data[1], &m_vBuffer[i * 7], 7);
		while (CAN_Transmit(CANLIB_CAN, &TxMessage) == CAN_TxStatus_NoMailBox);
	}

	/* Передать последнюю херню */
	if (bytes_to_send % 7) {
		TxMessage.StdId = GENID(DeviceID, CANMSG_DATA);
		TxMessage.ExtId = 0;
		TxMessage.IDE = CAN_Id_Standard;
		TxMessage.RTR = CAN_RTR_Data;
		TxMessage.DLC = 1 + bytes_to_send % 7;

		TxMessage.Data[0] = bytes_to_send / 7;
		memcpy(&TxMessage.Data[1], &m_vBuffer[bytes_to_send - bytes_to_send % 7], bytes_to_send % 7);
		while (CAN_Transmit(CANLIB_CAN, &TxMessage) == CAN_TxStatus_NoMailBox);
	}

	/* Ждем ACK или NACK */
	if (xQueueReceive(xCanQueueRx, &RxMessage, (TickType_t) 100) == pdTRUE) {
		if (GETMSG(RxMessage.StdId) == CANMSG_ACK)
			return CANError::CAN_OK;
		else
			return CANError::CAN_ACK_ERROR;

	} else {
		return CANError::CAN_TIMEOUT_ERROR;
	}

	return CANError::CAN_OK;
}

CANError CANTask::ProcessPkt() {
CanTxMsg lm;

	/* Send ACK */
	lm.StdId = GENID(DeviceID, CANMSG_ACK);
	lm.ExtId = 0;
	lm.IDE = CAN_Id_Standard;
	lm.RTR = CAN_RTR_Data;
	lm.DLC = 0;
	while (CAN_Transmit(CANLIB_CAN, &lm) == CAN_TxStatus_NoMailBox);
	return CANError::CAN_OK;
}

void CANTask::task() {
CanRxMsg RxMsg;

TimerHandle_t xTimer = xTimerCreate("Reset", 5000, pdFALSE, (void *)0, vTimerCallback);

	for (;;) {
		if (xQueueReceive(xCanQueueRx, &RxMsg, portMAX_DELAY) == pdTRUE) {
			switch (GETMSG(RxMsg.StdId)) {
			case CANMSG_SYNC:
				if (RxMsg.DLC == 0)
					SendToHost();
				else if (RxMsg.Data[0] == 0xFA) {
					/* Тут делаем сброс на загрузчик */
					RTC_WriteBackupRegister(RTC_BKP_DR0, 0xAABBCCDD);
					xTimerStart(xTimer, 0);
				}
				break;

			case CANMSG_HTD:
			case CANMSG_DATA:
				if (ReceiveFromHost(&RxMsg) == CANError::CAN_PKT_READY)
					ProcessPkt();
				break;

			default:
				break;
			}
		}
	}
}


/* Q - Скорость реакции */
FilterKalman::FilterKalman(float q, float r, float xEstLast, float PLast, float xEst)
	:Q(0.001f), R(0.01f), x_est_last(0.0f), P_last(24.0f), x_est(0.0f) {

	Q = q;
	R = r;
	x_est_last = xEstLast;
	P_last = PLast;
	x_est = xEst;
}


/*-----------------------------------------------------------------------------*/
FilterKalman::FilterKalman()
	:Q(0.001f), R(0.01f), x_est_last(0.0f), P_last(100.0f), x_est(0.0f) {
}


float FilterKalman::Process(float data_in) {
float x_temp_est;
float P_temp;
float K;

	x_temp_est = x_est_last;
	P_temp = P_last + Q;
	K = P_temp*(1.0f/(P_temp + R));
	x_est = x_temp_est + K*(data_in - x_temp_est);

	P_last = (1.0f - K)*P_temp;
	x_est_last = x_est;

	return x_est;
}

Pid::Pid(uint8_t index, unsigned portBASE_TYPE priority, uint16_t stackDepth) :
	m_fTemperature(20.0f) {

char buf[configMAX_TASK_NAME_LEN];
	m_iIndex = index;

	snprintf(buf, configMAX_TASK_NAME_LEN, "PID_%d", index);
	xQueueADCIn = xQueueCreate(2, sizeof(Pid::ADCin));
	m_xMutex = xSemaphoreCreateMutex();
	assert_param(m_xMutex);

	xTaskCreate(task_pid, buf, stackDepth, this, priority, &handle);
	assert_param(handle);
}


bool Pid::GetState(Pid::DataOut *dout) {
bool ret = false;
	assert_param(dout);

	if (xSemaphoreTake(m_xMutex, 2) == pdTRUE) {
		dout->Temperature = m_fTemperature;
		dout->sendiag = m_iSendiag;
		ret = true;
		xSemaphoreGive(m_xMutex);
	}
	return ret;
}


void Pid::task() {
Pid::ADCin ddin;

	lastTick = xTaskGetTickCount();

	for (;;) {
		if (xQueueReceive(xQueueADCIn, &ddin, portMAX_DELAY)) {
			if (m_iIndex == 0) // Первый АЦП - дергаем LED3
				LED1_PORT->ODR ^= LED1_PIN;

			if (ddin.sendiag == 0) {
				m_fTemperature = filter.Process(ddin.adc_data_uv);
			}
			m_iSendiag = ddin.sendiag;
		}
	}
}

/*
  Name  : CRC-32
  Poly  : 0x04C11DB7    x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11
                       + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1
  Init  : 0xFFFFFFFF
  Revert: true
  XorOut: 0xFFFFFFFF
  Check : 0xCBF43926 ("123456789")
  MaxLen: 268 435 455 байт (2 147 483 647 бит) - обнаружение
   одинарных, двойных, пакетных и всех нечетных ошибок
*/
const uint32_t Crc32Table[256] = {
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
    0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
    0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
    0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
    0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
    0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
    0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
    0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
    0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
    0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
    0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
    0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
    0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
    0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
    0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
    0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
    0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
    0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
    0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
    0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
    0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
    0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
    0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
    0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
    0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
    0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
    0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
    0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
    0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
    0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
    0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
    0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
    0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
    0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
    0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
    0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
    0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
    0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
    0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
    0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
    0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
    0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
    0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
    0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
    0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
    0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
    0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
    0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
    0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
    0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
    0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
    0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};

static uint32_t crc32(const unsigned char *buf, size_t len) {
	uint32_t crc = 0xFFFFFFFF;
	while(len--)
		crc = (crc >> 8) ^ Crc32Table[(crc ^ *buf++) & 0xFF];
	return crc ^ 0xFFFFFFFF;
}

/**
 * @function CAN1_RX0_IRQHandler
 * @breif Обработчик прерывания по получению сообщения CAN
 */
void CAN1_RX0_IRQHandler(void) {
CanRxMsg RxMessage;
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	CAN_Receive(CANLIB_CAN, CAN_FIFO0, &RxMessage);
	xQueueSendFromISR(canTask->xCanQueueRx, &RxMessage, &xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
