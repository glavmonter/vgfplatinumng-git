/*
 * maintask.cpp
 *
 *  Created on: 04 марта 2015 г.
 *      Author: supervisor
 */


#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "hardware.h"
#include "lmp90100.h"
#include "pid.h"
#include "tlsf.h"
#include "iadc.h"
#include "common.h"
#include "eeprom.h"
#include "diag/Trace.h"
#include "onewire.h"
#include "maintask.h"


static portTASK_FUNCTION_PROTO(vMainTask, pvParameters);

void InitSystem() {
	xTaskCreate(vMainTask, "Main", configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY, NULL);
}

static portTASK_FUNCTION(vMainTask, pvParameters) {
(void) pvParameters;

	//EEPROMInit();
	//RTCInit();

	portENTER_CRITICAL();
		Pid *pid;

		CANTask *canTask = new CANTask("CAN", configTASK_CAN_PRIORITY, configMINIMAL_STACK_SIZE*3);
		assert_param(canTask);
		CANLink(canTask);

		PIDInit();

		ADCsSetupSPI();

		LMP90100 *lmpADC_0_3 = new LMP90100("ADC1", configTASK_ADC_PRIORITY,
								configMINIMAL_STACK_SIZE*1, ADC1_SPI);
		assert_param(lmpADC_0_3);
		LMPLink(LMPIndex::LMPIndex_0_3, lmpADC_0_3);

		OneWire *wire = new OneWire("OW", configTASK_OW_PRIORITY, configMINIMAL_STACK_SIZE*2);
		assert_param(wire);
		canTask->m_xOneWire = wire;

		/* TODO Для запуска второго АЦП раскомментировать*/
//		LMP90100 *lmpADC_4_7 = new LMP90100("ADC1", configTASK_ADC_PRIORITY,
//								configMINIMAL_STACK_SIZE*1, ADC2_SPI);
//		assert_param(lmpADC_4_7);
//		LMPLink(LMPIndex::LMPIndex_4_7, lmpADC_4_7);

		/* Для младшего АЦП, зоны 0 - 3 */
		for (uint8_t i = 0; i < 3; i++) {
			pid = new Pid(i, configTASK_PID_PRIORITY, configMINIMAL_STACK_SIZE*2);
			assert_param(pid);

			canTask->m_lPids[i] = pid;
			lmpADC_0_3->m_lPids[i] = pid;
		}

	portEXIT_CRITICAL();

	for (;;) {
		vTaskDelay(portMAX_DELAY);
	}
}
