/*
 * Thermocouple.cpp
 *
 *  Created on: 16 июля 2014 г.
 *      Author: margel
 */


#include <FreeRTOS.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#include "Thermocouple.h"
#include "typeB.h"

static int32_t binsearch(const uint32_t *array, uint32_t array_size, uint32_t x) {
uint32_t first = 0;
uint32_t last = array_size;
	if (array_size == 0) {
		return -1;
	} else if (array[0] > x) {
		return -2;
	} else if (array[array_size - 1] < x) {
		return -3;
	}

	while (first < last) {
		uint32_t mid = first + (last - first)/2;
		if (x <= array[mid])
			last = mid;
		else
			first = mid + 1;
	}

	if (array[last] == x) {
		return last;
	} else {
		return last;
	}
}

float GetTemperature(double uV) {
#define TABLE_SIZE (sizeof(TypeB_uV)/sizeof(TypeB_uV[0]))

int32_t index;

	/* Возвращает индекс элемента или индекс ближайщего старшего */
	index = binsearch(TypeB_uV, TABLE_SIZE, (uint32_t)uV);
	if (index < 0)
		return NAN;

	if (index == 0)
		return 0.0f;

double T_lower = (double)TypeB_C[index - 1];
double T_upper = (double)TypeB_C[index];
double uV_lower = (double)TypeB_uV[index - 1];
double uV_upper = (double)TypeB_uV[index];

double Td = T_lower + ((double)uV - uV_lower)/(uV_upper - uV_lower)*(T_upper-T_lower);

	return (float)Td;
}
