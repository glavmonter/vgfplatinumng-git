/*
 * eeprom.cpp
 *
 *  Created on: 02 февр. 2015 г.
 *      Author: user5
 */


#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <timers.h>
#include <arm_math.h>
#include "hardware.h"
#include "diag/Trace.h"
#include "eeprom.h"

static portTASK_FUNCTION_PROTO(RTCTask, pvParameters);
static SemaphoreHandle_t xRTCSemaphore = NULL;

static EEPROM *eep = NULL;

void EEPROMInit() {
	eep = new EEPROM("EEP", tskIDLE_PRIORITY, configMINIMAL_STACK_SIZE*2);
}


EEPROM::EEPROM(char const *name, unsigned portBASE_TYPE priority, uint16_t stackDepth) :
	DeviceAddress(I2C_SLAVE_ADDRESS7) {

	EEP_Init();
	xTaskCreate(task_eep, name, stackDepth, (void *)this, priority, &handle);
}

bool EEPROM::TimeOutCallback(uint8_t index) {
	uart_printf("EEP Timeout: %d\n", index);
	for (;;) {}
	return false;
}

#define FULL_ARRAY 1024*8
void EEPROM::task() {
	uart_printf("EEP starting\n");
//	SelfTest();

	for (;;) {
		vTaskDelay(portMAX_DELAY);
	}
}

bool EEPROM::SelfTest() {
bool res = false;

	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);

	RNG_Cmd(ENABLE);
	CRC_ResetDR();

uint8_t *dout = new uint8_t[FULL_ARRAY];
uint32_t crc;
uint32_t *dout32 = (uint32_t *)dout;
TickType_t start, stop;

	for (uint32_t i = 0; i < FULL_ARRAY/4; i++) {
		dout32[i] = RNG_GetRandomNumber();
	}

	crc = CRC_CalcBlockCRC(dout32, FULL_ARRAY/4);

	start = xTaskGetTickCount();

	EEP_WriteBuffer(dout, 0x0000, FULL_ARRAY);
	EEP_WaitEepromStandbyState();


uint16_t NeedRead = FULL_ARRAY;
	memset(dout, 0, FULL_ARRAY);
	EEP_ReadBuffer(dout, 0x0000, &NeedRead);
	while (NeedRead > 0){}

	stop = xTaskGetTickCount();

	CRC_ResetDR();
uint32_t newcrc = CRC_CalcBlockCRC(dout32, FULL_ARRAY/4);

	if (newcrc != crc) {
		uart_printf("EEP rw error, time: %d \n", stop - start);
		res = false;
	}
	else {
		uart_printf("EEP rw ok, time: %d\n", stop - start);
		res = true;
	}

	delete []dout;

	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, DISABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, DISABLE);
	return res;
}

__IO uint32_t  sEETimeout = EEP_LONG_TIMEOUT;
__IO uint16_t* sEEDataReadPointer;
__IO uint8_t*  sEEDataWritePointer;
__IO uint8_t   sEEDataNum;

NVIC_InitTypeDef	NVIC_InitStructure;
DMA_InitTypeDef		DMA_InitStructure;


/**
  * @brief  Initializes DMA channel used by the I2C EEPROM driver.
  * @param  None
  * @retval None
  */
void EEPROM::EEP_LowLevel_DMAConfig(uint32_t pBuffer, uint32_t BufferSize, uint32_t Direction) {
	/* Initialize the DMA with the new parameters */
	if (Direction == EEP_DIRECTION_TX) {
		/* Configure the DMA Tx Stream with the buffer address and the buffer size */
		DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)pBuffer;
		DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
		DMA_InitStructure.DMA_BufferSize = (uint32_t)BufferSize;
		DMA_Init(EEP_I2C_DMA_STREAM_TX, &DMA_InitStructure);
	} else {
		/* Configure the DMA Rx Stream with the buffer address and the buffer size */
		DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)pBuffer;
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
		DMA_InitStructure.DMA_BufferSize = (uint32_t)BufferSize;
		DMA_Init(EEP_I2C_DMA_STREAM_RX, &DMA_InitStructure);
	}
}

/**
  * @brief  DeInitializes peripherals used by the I2C EEPROM driver.
  * @param  None
  * @retval None
  */
void EEPROM::EEP_LowLevel_DeInit(void) {
GPIO_InitTypeDef  GPIO_InitStructure;

	/* sEE_I2C Peripheral Disable */
	I2C_Cmd(EEP_I2C, DISABLE);

	/* sEE_I2C DeInit */
	I2C_DeInit(EEP_I2C);

	/*!< sEE_I2C Periph clock disable */
	RCC_APB1PeriphClockCmd(EEP_I2C_CLK, DISABLE);

	/*!< GPIO configuration */
	/*!< Configure sEE_I2C pins: SCL */
	GPIO_InitStructure.GPIO_Pin = EEP_I2C_SCL_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(EEP_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

	/*!< Configure sEE_I2C pins: SDA */
	GPIO_InitStructure.GPIO_Pin = EEP_I2C_SDA_PIN;
	GPIO_Init(EEP_I2C_SDA_GPIO_PORT, &GPIO_InitStructure);

	/* Configure and enable I2C DMA TX Stream interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EEP_I2C_DMA_TX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = EEP_I2C_DMA_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = EEP_I2C_DMA_SUBPRIO;
	NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Configure and enable I2C DMA RX Stream interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EEP_I2C_DMA_RX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = EEP_I2C_DMA_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = EEP_I2C_DMA_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Disable and Deinitialize the DMA Streams */
	DMA_Cmd(EEP_I2C_DMA_STREAM_TX, DISABLE);
	DMA_Cmd(EEP_I2C_DMA_STREAM_RX, DISABLE);
	DMA_DeInit(EEP_I2C_DMA_STREAM_TX);
	DMA_DeInit(EEP_I2C_DMA_STREAM_RX);
}

/**
  * @brief  Initializes peripherals used by the I2C EEPROM driver.
  * @param  None
  * @retval None
  */
void EEPROM::EEP_LowLevel_Init(void) {
GPIO_InitTypeDef  	GPIO_InitStructure;

	/*!< sEE_I2C Periph clock enable */
	RCC_APB1PeriphClockCmd(EEP_I2C_CLK, ENABLE);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Reset sEE_I2C IP */
	RCC_APB1PeriphResetCmd(EEP_I2C_CLK, ENABLE);

	/* Release reset signal of sEE_I2C IP */
	RCC_APB1PeriphResetCmd(EEP_I2C_CLK, DISABLE);

	/*!< GPIO configuration */
	/* Connect PXx to I2C_SCL*/
	GPIO_PinAFConfig(EEP_I2C_SCL_GPIO_PORT, EEP_I2C_SCL_SOURCE, EEP_I2C_SCL_AF);
	/* Connect PXx to I2C_SDA*/
	GPIO_PinAFConfig(EEP_I2C_SDA_GPIO_PORT, EEP_I2C_SDA_SOURCE, EEP_I2C_SDA_AF);

	/*!< Configure sEE_I2C pins: SCL */
	GPIO_InitStructure.GPIO_Pin = EEP_I2C_SCL_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(EEP_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

	/*!< Configure sEE_I2C pins: SDA */
	GPIO_InitStructure.GPIO_Pin = EEP_I2C_SDA_PIN;
	GPIO_Init(EEP_I2C_SDA_GPIO_PORT, &GPIO_InitStructure);


	/* Configure and enable I2C DMA TX Channel interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EEP_I2C_DMA_TX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = EEP_I2C_DMA_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = EEP_I2C_DMA_SUBPRIO;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Configure and enable I2C DMA RX Channel interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EEP_I2C_DMA_RX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = EEP_I2C_DMA_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = EEP_I2C_DMA_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/*!< I2C DMA TX and RX channels configuration */
	/* Enable the DMA clock */
	RCC_AHB1PeriphClockCmd(EEP_I2C_DMA_CLK, ENABLE);

	/* Clear any pending flag on Rx Stream  */
	DMA_ClearFlag(EEP_I2C_DMA_STREAM_TX, EEP_TX_DMA_FLAG_FEIF | EEP_TX_DMA_FLAG_DMEIF | EEP_TX_DMA_FLAG_TEIF | \
										EEP_TX_DMA_FLAG_HTIF | EEP_TX_DMA_FLAG_TCIF);
	/* Disable the EE I2C Tx DMA stream */
	DMA_Cmd(EEP_I2C_DMA_STREAM_TX, DISABLE);
	/* Configure the DMA stream for the EE I2C peripheral TX direction */
	DMA_DeInit(EEP_I2C_DMA_STREAM_TX);
	DMA_InitStructure.DMA_Channel = EEP_I2C_DMA_CHANNEL;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)EEP_I2C_DR_Address;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)0;    /* This parameter will be configured durig communication */;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral; /* This parameter will be configured durig communication */
	DMA_InitStructure.DMA_BufferSize = 0xFFFF;              /* This parameter will be configured durig communication */
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(EEP_I2C_DMA_STREAM_TX, &DMA_InitStructure);

	/* Clear any pending flag on Rx Stream */
	DMA_ClearFlag(EEP_I2C_DMA_STREAM_RX, EEP_RX_DMA_FLAG_FEIF | EEP_RX_DMA_FLAG_DMEIF | EEP_RX_DMA_FLAG_TEIF | \
										EEP_RX_DMA_FLAG_HTIF | EEP_RX_DMA_FLAG_TCIF);
	/* Disable the EE I2C DMA Rx stream */
	DMA_Cmd(EEP_I2C_DMA_STREAM_RX, DISABLE);
	/* Configure the DMA stream for the EE I2C peripheral RX direction */
	DMA_DeInit(EEP_I2C_DMA_STREAM_RX);
	DMA_Init(EEP_I2C_DMA_STREAM_RX, &DMA_InitStructure);

	/* Enable the DMA Channels Interrupts */
	DMA_ITConfig(EEP_I2C_DMA_STREAM_TX, DMA_IT_TC, ENABLE);
	DMA_ITConfig(EEP_I2C_DMA_STREAM_RX, DMA_IT_TC, ENABLE);
}

/**
  * @brief  DeInitializes peripherals used by the I2C EEPROM driver.
  * @param  None
  * @retval None
  */
void EEPROM::EEP_DeInit(void) {
	EEP_LowLevel_DeInit();
}

/**
  * @brief  Initializes peripherals used by the I2C EEPROM driver.
  * @param  None
  * @retval None
  */
void EEPROM::EEP_Init(void) {
I2C_InitTypeDef  I2C_InitStructure;

	EEP_LowLevel_Init();

	/*!< I2C configuration */
	/* EEP_I2C configuration */
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = DeviceAddress;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = I2C_SPEED;

	/* EEP_I2C Peripheral Enable */
	I2C_Cmd(EEP_I2C, ENABLE);
	/* Apply EEP_I2C configuration after enabling it */
	I2C_Init(EEP_I2C, &I2C_InitStructure);

	/* Enable the EEP_I2C peripheral DMA requests */
	I2C_DMACmd(EEP_I2C, ENABLE);
}

/**
  * @brief  Reads a block of data from the EEPROM.
  * @param  pBuffer : pointer to the buffer that receives the data read from
  *         the EEPROM.
  * @param  ReadAddr : EEPROM's internal address to start reading from.
  * @param  NumByteToRead : pointer to the variable holding number of bytes to
  *         be read from the EEPROM.
  *
  *        @note The variable pointed by NumByteToRead is reset to 0 when all the
  *              data are read from the EEPROM. Application should monitor this
  *              variable in order know when the transfer is complete.
  *
  * @note When number of data to be read is higher than 1, this function just
  *       configures the communication and enable the DMA channel to transfer data.
  *       Meanwhile, the user application may perform other tasks.
  *       When number of data to be read is 1, then the DMA is not used. The byte
  *       is read in polling mode.
  *
  * @retval EEP_OK (0) if operation is correctly performed, else return value
  *         different from EEP_OK (0) or the timeout user callback.
  */
uint32_t EEPROM::EEP_ReadBuffer(uint8_t *pBuffer, uint16_t ReadAddr, uint16_t *NumByteToRead) {
  /* Set the pointer to the Number of data to be read. This pointer will be used
      by the DMA Transfer Completer interrupt Handler in order to reset the
      variable to 0. User should check on this variable in order to know if the
      DMA transfer has been complete or not. */
	sEEDataReadPointer = NumByteToRead;

	/*!< While the bus is busy */
	sEETimeout = EEP_LONG_TIMEOUT;
	while (I2C_GetFlagStatus(EEP_I2C, I2C_FLAG_BUSY)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/*!< Send START condition */
	I2C_GenerateSTART(EEP_I2C, ENABLE);

	/*!< Test on EV5 and clear it (cleared by reading SR1 then writing to DR) */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_MODE_SELECT)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/*!< Send EEPROM address for write */
	I2C_Send7bitAddress(EEP_I2C, DeviceAddress, I2C_Direction_Transmitter);

	/*!< Test on EV6 and clear it */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

#ifdef EEP_M24C08

	/*!< Send the EEPROM's internal address to read from: Only one byte address */
	I2C_SendData(EEP_I2C, ReadAddr);

#elif defined (EEP_M24C64_32)

	/* Отправляем старший байт адреса */
	I2C_SendData(EEP_I2C, (uint8_t)((ReadAddr & 0xFF00) >> 8));

	/*!< Test on EV8 and clear it */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTING)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/* Отправляем младший байт адреса */
	I2C_SendData(EEP_I2C, (uint8_t)(ReadAddr & 0x00FF));

#endif /*!< EEP_M24C08 */

	/*!< Test on EV8 and clear it */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (I2C_GetFlagStatus(EEP_I2C, I2C_FLAG_BTF) == RESET) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/*!< Send STRAT condition a second time */
	I2C_GenerateSTART(EEP_I2C, ENABLE);

	/*!< Test on EV5 and clear it (cleared by reading SR1 then writing to DR) */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_MODE_SELECT)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/*!< Send EEPROM address for read */
	I2C_Send7bitAddress(EEP_I2C, DeviceAddress, I2C_Direction_Receiver);

	/* If number of data to be read is 1, then DMA couldn't be used */
	/* One Byte Master Reception procedure (POLLING) ---------------------------*/
	if ((uint16_t)(*NumByteToRead) < 2) {
		/* Wait on ADDR flag to be set (ADDR is still not cleared at this level */
		sEETimeout = EEP_FLAG_TIMEOUT;
		while (I2C_GetFlagStatus(EEP_I2C, I2C_FLAG_ADDR) == RESET) {
			if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
		}

		/*!< Disable Acknowledgement */
		I2C_AcknowledgeConfig(EEP_I2C, DISABLE);

		/* Clear ADDR register by reading SR1 then SR2 register (SR1 has already been read) */
		(void)EEP_I2C->SR2;

		/*!< Send STOP Condition */
		I2C_GenerateSTOP(EEP_I2C, ENABLE);

		/* Wait for the byte to be received */
		sEETimeout = EEP_FLAG_TIMEOUT;
		while (I2C_GetFlagStatus(EEP_I2C, I2C_FLAG_RXNE) == RESET) {
			if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
		}

		/*!< Read the byte received from the EEPROM */
		*pBuffer = I2C_ReceiveData(EEP_I2C);

		/*!< Decrement the read bytes counter */
		(uint16_t)(*NumByteToRead)--;

		/* Wait to make sure that STOP control bit has been cleared */
		sEETimeout = EEP_FLAG_TIMEOUT;
		while (EEP_I2C->CR1 & I2C_CR1_STOP) {
			if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
		}

		/*!< Re-Enable Acknowledgement to be ready for another reception */
		I2C_AcknowledgeConfig(EEP_I2C, ENABLE);
	} else {
		/* More than one Byte Master Reception procedure (DMA) -----------------*/
		/*!< Test on EV6 and clear it */
		sEETimeout = EEP_FLAG_TIMEOUT;
		while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) {
			if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
		}

		/* Configure the DMA Rx Channel with the buffer address and the buffer size */
		EEP_LowLevel_DMAConfig((uint32_t)pBuffer, (uint16_t)(*NumByteToRead), EEP_DIRECTION_RX);

		/* Inform the DMA that the next End Of Transfer Signal will be the last one */
		I2C_DMALastTransferCmd(EEP_I2C, ENABLE);

		/* Enable the DMA Rx Stream */
		DMA_Cmd(EEP_I2C_DMA_STREAM_RX, ENABLE);
	}

	/* If all operations OK, return sEE_OK (0) */
	return EEP_OK;
}

/**
  * @brief  Writes more than one byte to the EEPROM with a single WRITE cycle.
  *
  * @note   The number of bytes (combined to write start address) must not
  *         cross the EEPROM page boundary. This function can only write into
  *         the boundaries of an EEPROM page.
  *         This function doesn't check on boundaries condition (in this driver
  *         the function sEE_WriteBuffer() which calls sEE_WritePage() is
  *         responsible of checking on Page boundaries).
  *
  * @param  pBuffer : pointer to the buffer containing the data to be written to
  *         the EEPROM.
  * @param  WriteAddr : EEPROM's internal address to write to.
  * @param  NumByteToWrite : pointer to the variable holding number of bytes to
  *         be written into the EEPROM.
  *
  *        @note The variable pointed by NumByteToWrite is reset to 0 when all the
  *              data are written to the EEPROM. Application should monitor this
  *              variable in order know when the transfer is complete.
  *
  * @note This function just configure the communication and enable the DMA
  *       channel to transfer data. Meanwhile, the user application may perform
  *       other tasks in parallel.
  *
  * @retval EEP_OK (0) if operation is correctly performed, else return value
  *         different from EEP_OK (0) or the timeout user callback.
  */
uint32_t EEPROM::EEP_WritePage(uint8_t *pBuffer, uint16_t WriteAddr, uint8_t *NumByteToWrite)
{
	/* Set the pointer to the Number of data to be written. This pointer will be used
      by the DMA Transfer Completer interrupt Handler in order to reset the
      variable to 0. User should check on this variable in order to know if the
      DMA transfer has been complete or not. */
	sEEDataWritePointer = NumByteToWrite;

	/*!< While the bus is busy */
	sEETimeout = EEP_LONG_TIMEOUT;
	while (I2C_GetFlagStatus(EEP_I2C, I2C_FLAG_BUSY)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/*!< Send START condition */
	I2C_GenerateSTART(EEP_I2C, ENABLE);

	/*!< Test on EV5 and clear it */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_MODE_SELECT)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/*!< Send EEPROM address for write */
	sEETimeout = EEP_FLAG_TIMEOUT;
	I2C_Send7bitAddress(EEP_I2C, DeviceAddress, I2C_Direction_Transmitter);

	/*!< Test on EV6 and clear it */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

#ifdef EEP_M24C08

	/*!< Send the EEPROM's internal address to write to : only one byte Address */
	I2C_SendData(EEP_I2C, WriteAddr);

#elif defined(EEP_M24C64_32)

	/* Шлем старший байт адреса */
	I2C_SendData(EEP_I2C, (uint8_t)((WriteAddr & 0xFF00) >> 8));

	/*!< Test on EV8 and clear it */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTING)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/* Шлем младший байт адреса */
	I2C_SendData(EEP_I2C, (uint8_t)(WriteAddr & 0x00FF));

#endif /*!< sEE_M24C08 */

	/*!< Test on EV8 and clear it */
	sEETimeout = EEP_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTING)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

	/* Configure the DMA Tx Channel with the buffer address and the buffer size */
	EEP_LowLevel_DMAConfig((uint32_t)pBuffer, (uint8_t)(*NumByteToWrite), EEP_DIRECTION_TX);

	/* Enable the DMA Tx Stream */
	DMA_Cmd(EEP_I2C_DMA_STREAM_TX, ENABLE);

	/* If all operations OK, return sEE_OK (0) */
	return EEP_OK;
}

/**
  * @brief  Writes buffer of data to the I2C EEPROM.
  * @param  pBuffer : pointer to the buffer  containing the data to be written
  *         to the EEPROM.
  * @param  WriteAddr : EEPROM's internal address to write to.
  * @param  NumByteToWrite : number of bytes to write to the EEPROM.
  * @retval None
  */
void EEPROM::EEP_WriteBuffer(uint8_t *pBuffer, uint16_t WriteAddr, uint16_t NumByteToWrite)
{
uint16_t NumOfPage = 0, NumOfSingle = 0, count = 0;
uint16_t Addr = 0;

	Addr = WriteAddr % EEP_PAGESIZE;
	count = EEP_PAGESIZE - Addr;
	NumOfPage =  NumByteToWrite / EEP_PAGESIZE;
	NumOfSingle = NumByteToWrite % EEP_PAGESIZE;

	/*!< If WriteAddr is sEE_PAGESIZE aligned  */
	if (Addr == 0) {
		/*!< If NumByteToWrite < sEE_PAGESIZE */
		if (NumOfPage == 0) {
			/* Store the number of data to be written */
			sEEDataNum = NumOfSingle;
			/* Start writing data */
			EEP_WritePage(pBuffer, WriteAddr, (uint8_t *)(&sEEDataNum));
			/* Wait transfer through DMA to be complete */
			sEETimeout = EEP_LONG_TIMEOUT;
			while (sEEDataNum > 0) {
				if ((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
			}
			EEP_WaitEepromStandbyState();
		} else {
			/*!< If NumByteToWrite > sEE_PAGESIZE */
			while (NumOfPage--) {
				/* Store the number of data to be written */
				sEEDataNum = EEP_PAGESIZE;
				EEP_WritePage(pBuffer, WriteAddr, (uint8_t *)(&sEEDataNum));
				/* Wait transfer through DMA to be complete */
				sEETimeout = EEP_LONG_TIMEOUT;
				while (sEEDataNum > 0) {
					if ((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
				}
				EEP_WaitEepromStandbyState();
				WriteAddr +=  EEP_PAGESIZE;
				pBuffer += EEP_PAGESIZE;
			}

			if (NumOfSingle != 0) {
				/* Store the number of data to be written */
				sEEDataNum = NumOfSingle;
				EEP_WritePage(pBuffer, WriteAddr, (uint8_t *)(&sEEDataNum));
				/* Wait transfer through DMA to be complete */
				sEETimeout = EEP_LONG_TIMEOUT;
				while (sEEDataNum > 0) {
					if ((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
				}
				EEP_WaitEepromStandbyState();
			}
		}
	} else {
		/*!< If WriteAddr is not sEE_PAGESIZE aligned  */
		/*!< If NumByteToWrite < sEE_PAGESIZE */
		if (NumOfPage == 0) {
			/*!< If the number of data to be written is more than the remaining space
      	  	  in the current page: */
			if (NumByteToWrite > count) {
				/* Store the number of data to be written */
				sEEDataNum = count;
				/*!< Write the data contained in same page */
				EEP_WritePage(pBuffer, WriteAddr, (uint8_t *)(&sEEDataNum));
				/* Wait transfer through DMA to be complete */
				sEETimeout = EEP_LONG_TIMEOUT;
				while (sEEDataNum > 0) {
					if ((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
				}
				EEP_WaitEepromStandbyState();

				/* Store the number of data to be written */
				sEEDataNum = (NumByteToWrite - count);
				/*!< Write the remaining data in the following page */
				EEP_WritePage((uint8_t *)(pBuffer + count), (WriteAddr + count), (uint8_t *)(&sEEDataNum));
				/* Wait transfer through DMA to be complete */
				sEETimeout = EEP_LONG_TIMEOUT;
				while (sEEDataNum > 0) {
					if ((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
				}
				EEP_WaitEepromStandbyState();
			} else {
				/* Store the number of data to be written */
				sEEDataNum = NumOfSingle;
				EEP_WritePage(pBuffer, WriteAddr, (uint8_t *)(&sEEDataNum));
				/* Wait transfer through DMA to be complete */
				sEETimeout = EEP_LONG_TIMEOUT;
				while (sEEDataNum > 0) {
					if ((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
				}
				EEP_WaitEepromStandbyState();
			}
		} else {
			/*!< If NumByteToWrite > sEE_PAGESIZE */
			NumByteToWrite -= count;
			NumOfPage =  NumByteToWrite / EEP_PAGESIZE;
			NumOfSingle = NumByteToWrite % EEP_PAGESIZE;

			if (count != 0) {
				/* Store the number of data to be written */
				sEEDataNum = count;
				EEP_WritePage(pBuffer, WriteAddr, (uint8_t*)(&sEEDataNum));
				/* Wait transfer through DMA to be complete */
				sEETimeout = EEP_LONG_TIMEOUT;
				while (sEEDataNum > 0) {
					if((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
				}
				EEP_WaitEepromStandbyState();
				WriteAddr += count;
				pBuffer += count;
			}

			while (NumOfPage--) {
				/* Store the number of data to be written */
				sEEDataNum = EEP_PAGESIZE;
				EEP_WritePage(pBuffer, WriteAddr, (uint8_t *)(&sEEDataNum));
				/* Wait transfer through DMA to be complete */
				sEETimeout = EEP_LONG_TIMEOUT;
				while (sEEDataNum > 0) {
					if ((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
				}
				EEP_WaitEepromStandbyState();
				WriteAddr +=  EEP_PAGESIZE;
				pBuffer += EEP_PAGESIZE;
			}

			if (NumOfSingle != 0) {
				/* Store the number of data to be written */
				sEEDataNum = NumOfSingle;
				EEP_WritePage(pBuffer, WriteAddr, (uint8_t *)(&sEEDataNum));
				/* Wait transfer through DMA to be complete */
				sEETimeout = EEP_LONG_TIMEOUT;
				while (sEEDataNum > 0) {
					if ((sEETimeout--) == 0) {EEP_TIMEOUT_UserCallback(); return;};
				}
				EEP_WaitEepromStandbyState();
			}
		}
	}
}

/**
  * @brief  Wait for EEPROM Standby state.
  *
  * @note  This function allows to wait and check that EEPROM has finished the
  *        last operation. It is mostly used after Write operation: after receiving
  *        the buffer to be written, the EEPROM may need additional time to actually
  *        perform the write operation. During this time, it doesn't answer to
  *        I2C packets addressed to it. Once the write operation is complete
  *        the EEPROM responds to its address.
  *
  * @param  None
  * @retval sEE_OK (0) if operation is correctly performed, else return value
  *         different from sEE_OK (0) or the timeout user callback.
  */
uint32_t EEPROM::EEP_WaitEepromStandbyState(void)
{
__IO uint16_t tmpSR1 = 0;
__IO uint32_t sEETrials = 0;

	/*!< While the bus is busy */
	sEETimeout = EEP_LONG_TIMEOUT;
	while(I2C_GetFlagStatus(EEP_I2C, I2C_FLAG_BUSY)) {
		if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
	}

  /* Keep looping till the slave acknowledge his address or maximum number
     of trials is reached (this number is defined by sEE_MAX_TRIALS_NUMBER define
     in stm324xg_eval_i2c_ee.h file) */
	while (1) {
		/*!< Send START condition */
		I2C_GenerateSTART(EEP_I2C, ENABLE);

		/*!< Test on EV5 and clear it */
		sEETimeout = EEP_FLAG_TIMEOUT;
		while (!I2C_CheckEvent(EEP_I2C, I2C_EVENT_MASTER_MODE_SELECT)) {
			if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
		}

		/*!< Send EEPROM address for write */
		I2C_Send7bitAddress(EEP_I2C, DeviceAddress, I2C_Direction_Transmitter);

		/* Wait for ADDR flag to be set (Slave acknowledged his address) */
		sEETimeout = EEP_LONG_TIMEOUT;
		do {
			/* Get the current value of the SR1 register */
			tmpSR1 = EEP_I2C->SR1;

			/* Update the timeout value and exit if it reach 0 */
			if ((sEETimeout--) == 0) return EEP_TIMEOUT_UserCallback();
		}
		/* Keep looping till the Address is acknowledged or the AF flag is
       	   set (address not acknowledged at time) */
		while((tmpSR1 & (I2C_SR1_ADDR | I2C_SR1_AF)) == 0);

		/* Check if the ADDR flag has been set */
		if (tmpSR1 & I2C_SR1_ADDR) {
			/* Clear ADDR Flag by reading SR1 then SR2 registers (SR1 have already
         	 been read) */
			(void)EEP_I2C->SR2;

			/*!< STOP condition */
			I2C_GenerateSTOP(EEP_I2C, ENABLE);

			/* Exit the function */
			return EEP_OK;
		} else {
			/*!< Clear AF flag */
			I2C_ClearFlag(EEP_I2C, I2C_FLAG_AF);
		}

		/* Check if the maximum allowed number of trials has bee reached */
		if (sEETrials++ == EEP_MAX_TRIALS_NUMBER) {
			/* If the maximum number of trials has been reached, exit the function */
			return EEP_TIMEOUT_UserCallback();
		}
	}
	return EEP_OK;
}

/**
  * @brief  This function handles the DMA Tx Channel interrupt Handler.
  * @param  None
  * @retval None
  */
void EEP_I2C_DMA_TX_IRQHandler(void) {
	/* Check if the DMA transfer is complete */
	if (DMA_GetFlagStatus(EEP_I2C_DMA_STREAM_TX, EEP_TX_DMA_FLAG_TCIF) != RESET) {
		/* Disable the DMA Tx Stream and Clear TC flag */
		DMA_Cmd(EEP_I2C_DMA_STREAM_TX, DISABLE);
		DMA_ClearFlag(EEP_I2C_DMA_STREAM_TX, EEP_TX_DMA_FLAG_TCIF);

		/*!< Wait till all data have been physically transferred on the bus */
		sEETimeout = EEP_LONG_TIMEOUT;
		while (!I2C_GetFlagStatus(EEP_I2C, I2C_FLAG_BTF)) {
			if ((sEETimeout--) == 0) EEP_TIMEOUT_UserCallback();
		}

		/*!< Send STOP condition */
		I2C_GenerateSTOP(EEP_I2C, ENABLE);

		/* Reset the variable holding the number of data to be written */
		*sEEDataWritePointer = 0;
	}
}

/**
  * @brief  This function handles the DMA Rx Channel interrupt Handler.
  * @param  None
  * @retval None
  */
void EEP_I2C_DMA_RX_IRQHandler(void) {
	/* Check if the DMA transfer is complete */
	if (DMA_GetFlagStatus(EEP_I2C_DMA_STREAM_RX, EEP_RX_DMA_FLAG_TCIF) != RESET) {
		/*!< Send STOP Condition */
		I2C_GenerateSTOP(EEP_I2C, ENABLE);

		/* Disable the DMA Rx Stream and Clear TC Flag */
		DMA_Cmd(EEP_I2C_DMA_STREAM_RX, DISABLE);
		DMA_ClearFlag(EEP_I2C_DMA_STREAM_RX, EEP_RX_DMA_FLAG_TCIF);

		/* Reset the variable holding the number of data to be read */
		*sEEDataReadPointer = 0;
	}
}

#ifdef USE_DEFAULT_TIMEOUT_CALLBACK
/**
  * @brief  Basic management of the timeout situation.
  * @param  None.
  * @retval None.
  */
uint32_t EEP_TIMEOUT_UserCallback(void) {
	/* Block communication and all processes */
	while (1) {
	}
	return 0;
}
#endif /* USE_DEFAULT_TIMEOUT_CALLBACK */


void RTCInit() {
RTC_DateTypeDef RTC_DateStructure;
RTC_TimeTypeDef RTC_TimeStructure;
RTC_InitTypeDef RTC_InitStructure;
RTC_AlarmTypeDef RTC_AlarmStructure;
EXTI_InitTypeDef EXTI_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	PWR_BackupAccessCmd(ENABLE);

	RCC_LSEConfig(RCC_LSE_ON);
	while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) {
	}

	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
	RCC_RTCCLKCmd(ENABLE);
	RTC_WaitForSynchro();

	RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
	RTC_InitStructure.RTC_SynchPrediv = 0xFF;
	RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
	RTC_Init(&RTC_InitStructure);

	/* Set the alarm 00h:01min:15s */
	RTC_AlarmStructure.RTC_AlarmTime.RTC_H12     = RTC_H12_AM;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours   = 0x00;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0x01;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0x15;
	RTC_AlarmStructure.RTC_AlarmDateWeekDay = 1;
	RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
	RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay | RTC_AlarmMask_Hours | RTC_AlarmMask_Minutes; // Прерывание по будильнику, каждый час в 1:15

	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
	RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_AlarmStructure);

	RTC_ITConfig(RTC_IT_ALRA, ENABLE);
	RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
	RTC_ClearFlag(RTC_FLAG_ALRAF);

	RTC_EnterInitMode();
	/* Set the date: Sunday February 01th 2015 */
	RTC_DateStructure.RTC_Year = 0x15;
	RTC_DateStructure.RTC_Month = RTC_Month_February;
	RTC_DateStructure.RTC_Date = 0x01;
	RTC_DateStructure.RTC_WeekDay = RTC_Weekday_Sunday;
	RTC_SetDate(RTC_Format_BCD, &RTC_DateStructure);

	/* Set the time to 05h 20mn 00s AM */
	RTC_TimeStructure.RTC_H12 = RTC_H12_AM;
	RTC_TimeStructure.RTC_Hours = 0x00;
	RTC_TimeStructure.RTC_Minutes = 0x00;
	RTC_TimeStructure.RTC_Seconds = 0x00;
	RTC_SetTime(RTC_Format_BCD, &RTC_TimeStructure);
	RTC_ExitInitMode();

	/* EXTI configuration *********************************************************/
	EXTI_ClearITPendingBit(EXTI_Line17);
	EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable the RTC Alarm Interrupt, самый низший приоритет */
	NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = configLIBRARY_LOWEST_INTERRUPT_PRIORITY;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	xRTCSemaphore = xSemaphoreCreateBinary();
	xSemaphoreTake(xRTCSemaphore, 0);

	xTaskCreate(RTCTask, "RTC", configMINIMAL_STACK_SIZE*1, NULL, tskIDLE_PRIORITY, NULL);
}


static portTASK_FUNCTION(RTCTask, pvParameters) {
(void)pvParameters;

RTC_TimeTypeDef RTC_TimeStructure;
RTC_AlarmTypeDef RTC_AlarmStructure;

	for (;;) {
		if (xSemaphoreTake(xRTCSemaphore, 1000) == pdTRUE) {
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
			RTC_GetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);

			uart_printf("\nAlarm: %0.2d:%0.2d:%0.2d\n\n", RTC_TimeStructure.RTC_Hours,
							RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
		}
	}
}

/**
  * @brief  This function handles RTC Alarms interrupt request.
  * @param  None
  * @retval None
  */
void RTC_Alarm_IRQHandler(void)
{
static BaseType_t xHigherPriorityTaskWoken;

	if(RTC_GetITStatus(RTC_IT_ALRA) != RESET) {
		RTC_ClearITPendingBit(RTC_IT_ALRA);
	    EXTI_ClearITPendingBit(EXTI_Line17);

	    xSemaphoreGiveFromISR(xRTCSemaphore, &xHigherPriorityTaskWoken);
	    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}
