/*
 *  @file VGFThermo/hardware.h
 *  @date 30.12.2012
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief Описание выводов, и портов
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_
#include "stm32f4xx.h"


/* Dual color LEDS */
#define LEDn								4

#define LED1
#define LED1_PIN							GPIO_Pin_1
#define LED1_PORT							GPIOE
#define LED1_CLK							RCC_AHB1Periph_GPIOE

#define LED2
#define LED2_PIN							GPIO_Pin_0
#define LED2_PORT							GPIOE
#define LED2_CLK							RCC_AHB1Periph_GPIOE

#define LED3
#define LED3_PIN							GPIO_Pin_9
#define LED3_PORT							GPIOB
#define LED3_CLK							RCC_AHB1Periph_GPIOB

#define LED4
#define LED4_PIN							GPIO_Pin_8
#define LED4_PORT							GPIOB
#define LED4_CLK							RCC_AHB1Periph_GPIOB

#define LED_RED(led)						GPIO_SetBits(led##_PORT, led##_PIN)
#define LED_GREEN(led)						GPIO_ResetBits(led##_PORT, led##_PIN)

/* ADC 1 (left) */
#define ADC1_SPI							SPI2
#define ADC1_SPI_CLK						RCC_APB1Periph_SPI2
#define ADC1_SPI_CLK_INIT					RCC_APB1PeriphClockCmd

/* SPI2 SCK, PB13 */
#define ADC1_SPI_SCK_PIN					GPIO_Pin_13
#define ADC1_SPI_SCK_GPIO_PORT				GPIOB
#define ADC1_SPI_SCK_GPIO_CLK				RCC_AHB1Periph_GPIOB
#define ADC1_SPI_SCK_SOURCE					GPIO_PinSource13
#define ADC1_SPI_SCK_AF						GPIO_AF_SPI2

/* SPI2 MISO, PB14 */
#define ADC1_SPI_MISO_PIN					GPIO_Pin_14
#define ADC1_SPI_MISO_GPIO_PORT				GPIOB
#define ADC1_SPI_MISO_GPIO_CLK				RCC_AHB1Periph_GPIOB
#define ADC1_SPI_MISO_SOURCE				GPIO_PinSource14
#define ADC1_SPI_MISO_AF					GPIO_AF_SPI2

/* SPI2 MOSI, PB15 */
#define ADC1_SPI_MOSI_PIN					GPIO_Pin_15
#define ADC1_SPI_MOSI_GPIO_PORT				GPIOB
#define ADC1_SPI_MOSI_GPIO_CLK				RCC_AHB1Periph_GPIOB
#define ADC1_SPI_MOSI_SOURCE				GPIO_PinSource15
#define ADC1_SPI_MOSI_AF					GPIO_AF_SPI2

/* SPI2 NSS, PB14 */
#define ADC1_SPI_NSS_PIN					GPIO_Pin_12
#define ADC1_SPI_NSS_GPIO_PORT				GPIOB
#define ADC1_SPI_NSS_GPIO_CLK				RCC_AHB1Periph_GPIOB

/* SPI2 DRDY, PD1 */
#define ADC1_DRDY_PIN						GPIO_Pin_1
#define ADC1_DRDY_GPIO_PORT					GPIOD
#define ADC1_DRDY_GPIO_CLK					RCC_AHB1Periph_GPIOD
#define ADC1_DRDY_EXTI_SOURCE				EXTI_PortSourceGPIOD
#define ADC1_DRDY_EXTI_PIN					EXTI_PinSource1

/* ADC 2 (right) */
#define ADC2_SPI							SPI3
#define ADC2_SPI_CLK						RCC_APB1Periph_SPI3
#define ADC2_SPI_CLK_INIT					RCC_APB1PeriphClockCmd

/* SPI3 SCK, PC10 */
#define ADC2_SPI_SCK_PIN					GPIO_Pin_10
#define ADC2_SPI_SCK_GPIO_PORT				GPIOC
#define ADC2_SPI_SCK_GPIO_CLK				RCC_AHB1Periph_GPIOC
#define ADC2_SPI_SCK_SOURCE					GPIO_PinSource10
#define ADC2_SPI_SCK_AF						GPIO_AF_SPI3

/* SPI3 MISO, PC11 */
#define ADC2_SPI_MISO_PIN					GPIO_Pin_11
#define ADC2_SPI_MISO_GPIO_PORT				GPIOC
#define ADC2_SPI_MISO_GPIO_CLK				RCC_AHB1Periph_GPIOC
#define ADC2_SPI_MISO_SOURCE				GPIO_PinSource11
#define ADC2_SPI_MISO_AF					GPIO_AF_SPI3

/* SPI3 MOSI, PC12 */
#define ADC2_SPI_MOSI_PIN					GPIO_Pin_12
#define ADC2_SPI_MOSI_GPIO_PORT				GPIOC
#define ADC2_SPI_MOSI_GPIO_CLK				RCC_AHB1Periph_GPIOC
#define ADC2_SPI_MOSI_SOURCE				GPIO_PinSource12
#define ADC2_SPI_MOSI_AF					GPIO_AF_SPI3

/* SPI3 NSS, PA15 */
#define ADC2_SPI_NSS_PIN					GPIO_Pin_15
#define ADC2_SPI_NSS_GPIO_PORT				GPIOA
#define ADC2_SPI_NSS_GPIO_CLK				RCC_AHB1Periph_GPIOA

/* SPI3 DRDY, PD0 */
#define ADC2_DRDY_PIN						GPIO_Pin_0
#define ADC2_DRDY_GPIO_PORT					GPIOD
#define ADC2_DRDY_GPIO_CLK					RCC_AHB1Periph_GPIOD
#define ADC2_DRDY_EXTI_SOURCE				EXTI_PortSourceGPIOD
#define ADC2_DRDY_EXTI_PIN					EXTI_PinSource0


/* Debug port (USART1) */
#define DBG_USART							USART1
#define DBG_USART_CLK						RCC_APB2Periph_USART1
#define DBG_USART_CLK_INIT					RCC_APB2PeriphClockCmd

/* USART1 Tx, PA9 */
#define DBG_USART_TX_PIN					GPIO_Pin_9
#define DBG_USART_TX_GPIO_PORT				GPIOA
#define DBG_USART_TX_GPIO_CLK				RCC_AHB1Periph_GPIOA
#define DBG_USART_TX_SOURCE					GPIO_PinSource9
#define DBG_USART_TX_AF						GPIO_AF_USART1

/* USART1 Rx, PA10 */
#define DBG_USART_RX_PIN					GPIO_Pin_10
#define DBG_USART_RX_GPIO_PORT				GPIOA
#define DBG_USART_RX_GPIO_CLK				RCC_AHB1Periph_GPIOA
#define DBG_USART_RX_SOURCE					GPIO_PinSource10
#define DBG_USART_RX_AF						GPIO_AF_USART1


/* 1-Wire port (USART2) */
#define WIRE_USART							USART2
#define WIRE_USART_CLK						RCC_APB1Periph_USART2
#define WIRE_USART_CLK_INIT					RCC_APB1PeriphClockCmd

/* 1-Wire USART2 Tx, PA2 */
#define WIRE_USART_TX_PIN					GPIO_Pin_2
#define WIRE_USART_TX_GPIO_PORT				GPIOA
#define WIRE_USART_TX_GPIO_CLK				RCC_AHB1Periph_GPIOA
#define WIRE_USART_TX_SOURCE				GPIO_PinSource2
#define WIRE_USART_TX_AF					GPIO_AF_USART2

/* 1-Wire USART2 Rx, PA3 */
#define WIRE_USART_RX_PIN					GPIO_Pin_3
#define WIRE_USART_RX_GPIO_PORT				GPIOA
#define WIRE_USART_RX_GPIO_CLK				RCC_AHB1Periph_GPIOA
#define WIRE_USART_RX_SOURCE				GPIO_PinSource3
#define WIRE_USART_RX_AF					GPIO_AF_USART2


/* Address encoder */
#define ENC_S1_PIN							GPIO_Pin_2
#define ENC_S1_GPIO_PORT					GPIOE
#define ENC_S1_GPIO_CLK						RCC_AHB1Periph_GPIOE

#define ENC_S2_PIN							GPIO_Pin_3
#define ENC_S2_GPIO_PORT					GPIOE
#define ENC_S2_GPIO_CLK						RCC_AHB1Periph_GPIOE

#define ENC_S4_PIN							GPIO_Pin_4
#define ENC_S4_GPIO_PORT					GPIOE
#define ENC_S4_GPIO_CLK						RCC_AHB1Periph_GPIOE

#define ENC_S8_PIN							GPIO_Pin_5
#define ENC_S8_GPIO_PORT					GPIOE
#define ENC_S8_GPIO_CLK						RCC_AHB1Periph_GPIOE


/* I2C EEPROM */
#define EEP_I2C								I2C1
#define EEP_I2C_CLK							RCC_APB1Periph_I2C1
#define EEP_I2C_CLK_INIT					RCC_APB1PeriphClockCmd

/* I2C1 SCL, PB6 */
#define EEP_I2C_SCL_PIN						GPIO_Pin_6
#define EEP_I2C_SCL_GPIO_PORT				GPIOB
#define EEP_I2C_SCL_GPIO_CLK				RCC_AHB1Periph_GPIOB
#define EEP_I2C_SCL_SOURCE					GPIO_PinSource6
#define EEP_I2C_SCL_AF						GPIO_AF_I2C1

/* I2C1 SDA, PB7 */
#define EEP_I2C_SDA_PIN						GPIO_Pin_7
#define EEP_I2C_SDA_GPIO_PORT				GPIOB
#define EEP_I2C_SDA_GPIO_CLK				RCC_AHB1Periph_GPIOB
#define EEP_I2C_SDA_SOURCE					GPIO_PinSource7
#define EEP_I2C_SDA_AF						GPIO_AF_I2C1

#define EEP_I2C_DMA                      	DMA1
#define EEP_I2C_DMA_CHANNEL              	DMA_Channel_1
#define EEP_I2C_DMA_STREAM_TX            	DMA1_Stream6
#define EEP_I2C_DMA_STREAM_RX            	DMA1_Stream0
#define EEP_I2C_DMA_CLK                  	RCC_AHB1Periph_DMA1
#define EEP_I2C_DR_Address               	((uint32_t)0x40005410)
#define EEP_USE_DMA

#define EEP_I2C_DMA_TX_IRQn              	DMA1_Stream6_IRQn
#define EEP_I2C_DMA_RX_IRQn              	DMA1_Stream0_IRQn
#define EEP_I2C_DMA_TX_IRQHandler        	DMA1_Stream6_IRQHandler
#define EEP_I2C_DMA_RX_IRQHandler        	DMA1_Stream0_IRQHandler
#define EEP_I2C_DMA_PREPRIO              	0
#define EEP_I2C_DMA_SUBPRIO              	0

#define EEP_TX_DMA_FLAG_FEIF             	DMA_FLAG_FEIF6
#define EEP_TX_DMA_FLAG_DMEIF            	DMA_FLAG_DMEIF6
#define EEP_TX_DMA_FLAG_TEIF             	DMA_FLAG_TEIF6
#define EEP_TX_DMA_FLAG_HTIF             	DMA_FLAG_HTIF6
#define EEP_TX_DMA_FLAG_TCIF             	DMA_FLAG_TCIF6
#define EEP_RX_DMA_FLAG_FEIF             	DMA_FLAG_FEIF0
#define EEP_RX_DMA_FLAG_DMEIF            	DMA_FLAG_DMEIF0
#define EEP_RX_DMA_FLAG_TEIF             	DMA_FLAG_TEIF0
#define EEP_RX_DMA_FLAG_HTIF             	DMA_FLAG_HTIF0
#define EEP_RX_DMA_FLAG_TCIF             	DMA_FLAG_TCIF0

#define EEP_DIRECTION_TX                 	0
#define EEP_DIRECTION_RX                 	1

/* External watchdog WDI, PA0 */
#define WDI_PIN								GPIO_Pin_0
#define WDI_GPIO_PORT						GPIOA
#define WDI_GPIO_CLK						RCC_AHB1Periph_GPIOA


/* CAN1 */
#define CANLIB_CAN							CAN1
#define CAN_CLK								RCC_APB1Periph_CAN1
#define CAN_CLK_INIT						RCC_APB1PeriphClockCmd

/* CAN1 RX, PA11 */
#define CAN_RX_PIN							GPIO_Pin_11
#define CAN_RX_GPIO_PORT					GPIOA
#define CAN_RX_GPIO_CLK						RCC_AHB1Periph_GPIOA
#define CAN_RX_SOURCE						GPIO_PinSource11
#define CAN_RX_AF							GPIO_AF_CAN1

/* CAN1 TX, PA12 */
#define CAN_TX_PIN							GPIO_Pin_12
#define CAN_TX_GPIO_PORT					GPIOA
#define CAN_TX_GPIO_CLK						RCC_AHB1Periph_GPIOA
#define CAN_TX_SOURCE						GPIO_PinSource12
#define CAN_TX_AF							GPIO_AF_CAN1


/*  TIM1, CH[3..0] */
/* TIM1_CH1, PE9 */
#define TIM1_CH0_PIN						GPIO_Pin_9
#define TIM1_CH0_GPIO_PORT					GPIOE
#define TIM1_CH0_GPIO_CLK					RCC_AHB1Periph_GPIOE
#define TIM1_CH0_SOURCE						GPIO_PinSource9
#define TIM1_CH0_AF							GPIO_AF_TIM1

/* TIM1_CH2, PE11 */
#define TIM1_CH1_PIN						GPIO_Pin_11
#define TIM1_CH1_GPIO_PORT					GPIOE
#define TIM1_CH1_GPIO_CLK					RCC_AHB1Periph_GPIOE
#define TIM1_CH1_SOURCE						GPIO_PinSource11
#define TIM1_CH1_AF							GPIO_AF_TIM1

/* TIM1_CH3, PE13 */
#define TIM1_CH2_PIN						GPIO_Pin_13
#define TIM1_CH2_GPIO_PORT					GPIOE
#define TIM1_CH2_GPIO_CLK					RCC_AHB1Periph_GPIOE
#define TIM1_CH2_SOURCE						GPIO_PinSource13
#define TIM1_CH2_AF							GPIO_AF_TIM1

/* TIM1_CH4, PE14 */
#define TIM1_CH3_PIN						GPIO_Pin_14
#define TIM1_CH3_GPIO_PORT					GPIOE
#define TIM1_CH3_GPIO_CLK					RCC_AHB1Periph_GPIOE
#define TIM1_CH3_SOURCE						GPIO_PinSource14
#define TIM1_CH3_AF							GPIO_AF_TIM1

/* TIM8, CH[7..4] */
/* TIM8_CH1, PC6 */
#define TIM8_CH0_PIN						GPIO_Pin_6
#define TIM8_CH0_GPIO_PORT					GPIOC
#define TIM8_CH0_GPIO_CLK					RCC_AHB1Periph_GPIOC
#define TIM8_CH0_SOURCE						GPIO_PinSource6
#define TIM8_CH0_AF							GPIO_AF_TIM8

/* TIM8_CH2, PC7 */
#define TIM8_CH1_PIN						GPIO_Pin_7
#define TIM8_CH1_GPIO_PORT					GPIOC
#define TIM8_CH1_GPIO_CLK					RCC_AHB1Periph_GPIOC
#define TIM8_CH1_SOURCE						GPIO_PinSource7
#define TIM8_CH1_AF							GPIO_AF_TIM8

/* TIM8_CH3, PC8 */
#define TIM8_CH2_PIN						GPIO_Pin_8
#define TIM8_CH2_GPIO_PORT					GPIOC
#define TIM8_CH2_GPIO_CLK					RCC_AHB1Periph_GPIOC
#define TIM8_CH2_SOURCE						GPIO_PinSource8
#define TIM8_CH2_AF							GPIO_AF_TIM8

/* TIM8_CH4, PC9 */
#define TIM8_CH3_PIN						GPIO_Pin_9
#define TIM8_CH3_GPIO_PORT					GPIOC
#define TIM8_CH3_GPIO_CLK					RCC_AHB1Periph_GPIOC
#define TIM8_CH3_SOURCE						GPIO_PinSource9
#define TIM8_CH3_AF							GPIO_AF_TIM8


#endif /* HARDWARE_H_ */
