/*
 * onewire.h
 *
 *  Created on: 12 нояб. 2013 г.
 *      Author: sony
 */

#ifndef ONEWIRE_H_
#define ONEWIRE_H_

#include <stm32f4xx.h>
#include "common.h"

#define BAUD_RATE_RESET		9600
#define BAUD_RATE_NORMAL	115200

#define OW_DEVICES_YES		0
#define OW_DEVICES_NO		1

#define OW_ROM_MATCH		0x55
#define OW_ROM_SKIP			0xCC
#define OW_ROM_READ			0x33

typedef struct {
	uint8_t Index;
	uint8_t ROM[8];
	float Temperature;
} DS12B20Device;

void OW_Init();
uint8_t OW_Reset();
void OW_WriteByte(uint8_t data);


#define DS12B80_COUNT	9

class OneWire : public TaskBase {
public:
	OneWire(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);

	void task();
	static void task_onewire(void *param) {
		static_cast<OneWire *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	bool GetTemperatures(float *T);

private:
	SemaphoreHandle_t xMutex;

	void OW_Init();
	uint8_t OW_Reset();
	void OW_WriteByte(uint8_t data);
	uint8_t OW_ReadByte();
	uint8_t OW_CalcScratchCRC(uint8_t *data);
	uint8_t OW_ComputeCRC8(unsigned char inData, unsigned char seed);

	float ReadTemperature(DS12B20Device *device);

};

#endif /* ONEWIRE_H_ */
