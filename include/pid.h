/*
 *  @file VGFThermo/pid.c
 *  @date 14.01.2013
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief Процесс расчета ПИД регулятора и обслуживания таймеров
 */
#ifndef PID_H_
#define PID_H_

#include <arm_math.h>
#include <stm32f4xx.h>
#include <FreeRTOS.h>
#include <queue.h>

#include "common.h"
#include "VGFPlatinum.pb.h"
#include "encid.h"
#include "Thermocouple.h"
#include "onewire.h"

typedef enum {
	CS_WAIT_START = 0,
	CS_SEND_TO_HOST = 1,
	CS_RECEIVE_FROM_HOST = 2
} CANStatus;


void PIDUpdate(double adc_data_uV, int num_zone, uint8_t sendiag);
void PIDSetError();
void PIDInit();

float ADCtoCelsius(int32_t adc_data);

typedef enum {
	CAN_OK = 0,
	CAN_PKT_READY,
	CAN_INDEX_TO_LARGE,
	CAN_ERROR_PB,
	CAN_TIMEOUT_ERROR,
	CAN_ACK_ERROR,
	CAN_MEMORY_ERROR,
	CAN_SEMPTH_ERROR,
	CAN_ERROR
} CANError;


class Pid;
class OneWire;
class FilterKalman;
class CANTask;

void CANLink(CANTask *l);

class CANTask : public TaskBase {
public:
	CANTask(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);

	void task();
	static void task_can(void *param) {
		static_cast<CANTask *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	Pid *m_lPids[8];
	OneWire *m_xOneWire;

	QueueHandle_t xCanQueueRx;

private:
	void InitCan();
	CANError ReceiveFromHost(CanRxMsg *RxMessage);
	CANError SendToHost();
	CANError ProcessPkt();

	uint8_t DeviceID;
	uint16_t m_iBytesToReceive;
	uint8_t m_iMessagesToReceive;
	uint8_t m_iMaxMessageIndex;
	uint32_t m_iCrcIn;

	uint8_t m_vBuffer[Payload_size];
	Payload m_xPayload;
};


class FilterKalman {
public:
	FilterKalman();
	FilterKalman(float q, float r, float xEstLast, float PLast, float xEst);

	float Q;
	float R;
	float Process(float data_in);

private:
	float x_est_last;
	float P_last;
	float x_est;
};


class Pid : public TaskBase {
public:
	typedef struct {
		double adc_data_uv;
		uint8_t sendiag;
	} ADCin;

	typedef struct {
		float Temperature;
		uint8_t sendiag;
	} DataOut;

public:
	Pid(uint8_t index, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);

	void task();
	static void task_pid(void *param) {
		static_cast<Pid *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	QueueHandle_t xQueueADCIn;


	bool GetState(Pid::DataOut *dout);

private:
	float m_fTemperature;
	uint8_t m_iSendiag;

	FilterKalman filter;
	SemaphoreHandle_t m_xMutex;
	uint8_t m_iIndex;
	TickType_t lastTick;

};


#ifdef __cplusplus
extern "C" {
#endif

void CAN1_RX0_IRQHandler(void);

#ifdef __cplusplus
}
#endif


#endif /* PID_H_ */
