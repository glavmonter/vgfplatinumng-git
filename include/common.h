#ifndef COMMON_H_
#define COMMON_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <new>
#include <stm32f4xx.h>


class TaskBase {
public:
	TaskHandle_t handle;
	virtual ~TaskBase() {
#if INCLUDE_vTaskDelete
		vTaskDelete(handle);
#endif
		return;
	}
};


#define CAN_MUTEX_TIMEOUT	25

bool CanSendSem(CanTxMsg *msg);

char *FloatToString(char *outstr, double value, int places, int minwidth = 0, bool rightjustify = false);

#ifdef __cplusplus
extern "C" {
#endif

void vApplicationTickHook();

#ifdef __cplusplus
}
#endif

#endif /* COMMON_H_ */
